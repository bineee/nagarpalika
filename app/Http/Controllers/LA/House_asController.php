<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\House_a;

class House_asController extends Controller
{
    public $show_action = true;
    
    /**
     * Display a listing of the House_as.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('House_as');
        
        if(Module::hasAccess($module->id)) {
            return View('la.house_as.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('House_as'),
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for creating a new house_a.
     *
     * @return mixed
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created house_a in database.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("House_as", "create")) {
            
            $rules = Module::validateRules("House_as", $request);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            
            $insert_id = Module::insert("House_as", $request);
            
            return redirect()->route(config('laraadmin.adminRoute') . '.house_as.index');
            
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Display the specified house_a.
     *
     * @param int $id house_a ID
     * @return mixed
     */
    public function show($id)
    {
        if(Module::hasAccess("House_as", "view")) {
            
            $house_a = House_a::find($id);
            if(isset($house_a->id)) {
                $module = Module::get('House_as');
                $module->row = $house_a;
                
                return view('la.house_as.show', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('house_a', $house_a);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("house_a"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for editing the specified house_a.
     *
     * @param int $id house_a ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        if(Module::hasAccess("House_as", "edit")) {
            $house_a = House_a::find($id);
            if(isset($house_a->id)) {
                $module = Module::get('House_as');
                
                $module->row = $house_a;
                
                return view('la.house_as.edit', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                ])->with('house_a', $house_a);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("house_a"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Update the specified house_a in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id house_a ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("House_as", "edit")) {
            
            $rules = Module::validateRules("House_as", $request, true);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();;
            }
            
            $insert_id = Module::updateRow("House_as", $request, $id);
            
            return redirect()->route(config('laraadmin.adminRoute') . '.house_as.index');
            
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Remove the specified house_a from storage.
     *
     * @param int $id house_a ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Module::hasAccess("House_as", "delete")) {
            House_a::find($id)->delete();
            
            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.house_as.index');
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Server side Datatable fetch via Ajax
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function dtajax(Request $request)
    {
        $module = Module::get('House_as');
        $listing_cols = Module::getListingColumns('House_as');
        
        $values = DB::table('house_as')->select($listing_cols)->whereNull('deleted_at');
        $out = Datatables::of($values)->make();
        $data = $out->getData();
        
        $fields_popup = ModuleFields::getModuleFields('House_as');
        
        for($i = 0; $i < count($data->data); $i++) {
            for($j = 0; $j < count($listing_cols); $j++) {
                $col = $listing_cols[$j];
                if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $module->view_col) {
                    $data->data[$i][$j] = '<a href="' . url(config('laraadmin.adminRoute') . '/house_as/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
                }
                // else if($col == "author") {
                //    $data->data[$i][$j];
                // }
            }
            
            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("House_as", "edit")) {
                    $output .= '<a href="' . url(config('laraadmin.adminRoute') . '/house_as/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
                }
                
                if(Module::hasAccess("House_as", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.house_as.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }
        $out->setData($data);
        return $out;
    }
}
