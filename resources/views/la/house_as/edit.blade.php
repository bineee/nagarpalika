@extends("la.layouts.app")

@section("contentheader_title")
    <a href="{{ url(config('laraadmin.adminRoute') . '/house_as') }}">House a</a> :
@endsection
@section("contentheader_description", $house_a->$view_col)
@section("section", "House as")
@section("section_url", url(config('laraadmin.adminRoute') . '/house_as'))
@section("sub_section", "Edit")

@section("htmlheader_title", "House as Edit : ".$house_a->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Form::model($house_a, ['route' => [config('laraadmin.adminRoute') . '.house_as.update', $house_a->id ], 'method'=>'PUT', 'id' => 'house_a-edit-form']) !!}
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'name')
					@la_input($module, 'name_loc')
					@la_input($module, 'status')
                    --}}
                    <br>
                    <div class="form-group">
                        {!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <a href="{{ url(config('laraadmin.adminRoute') . '/house_as') }}" class="btn btn-default pull-right">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
    $("#house_a-edit-form").validate({
        
    });
});
</script>
@endpush
