@extends("la.layouts.app")

@section("contentheader_title")
    <a href="{{ url(config('laraadmin.adminRoute') . '/yes_nos') }}">Yes No</a> :
@endsection
@section("contentheader_description", $yes_no->$view_col)
@section("section", "Yes Nos")
@section("section_url", url(config('laraadmin.adminRoute') . '/yes_nos'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Yes Nos Edit : ".$yes_no->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Form::model($yes_no, ['route' => [config('laraadmin.adminRoute') . '.yes_nos.update', $yes_no->id ], 'method'=>'PUT', 'id' => 'yes_no-edit-form']) !!}
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'name')
                    --}}
                    <br>
                    <div class="form-group">
                        {!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <a href="{{ url(config('laraadmin.adminRoute') . '/yes_nos') }}" class="btn btn-default pull-right">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
    $("#yes_no-edit-form").validate({
        
    });
});
</script>
@endpush
